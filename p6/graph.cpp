/* implementation of graph class. */

/* TODO: write this. */

#include <fstream>

#include "graph.h"

/**
 * constructor
 */
Graph::Graph(){
    nodes.clear();
    isDirected = true;
    // add predefined color to the graph
    colors.push_back("skyblue");
    colors.push_back("goldenrod");
    colors.push_back("greenyellow");
    colors.push_back("lightcyan");
    colors.push_back("orange");
    colors.push_back("saddlebrown");
    colors.push_back("lightskyblue");
    colors.push_back("palevioletred");
    colors.push_back("lightgray");
    colors.push_back("mintcream");    
    colors.push_back("springgreen");
    colors.push_back("beige");
    colors.push_back("firebrick");
    colors.push_back("dimgray");
    colors.push_back("tomato");
     colors.push_back("violetred");
    
    
};
/**
 * destructor
 */
Graph::~Graph(){
    for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it)
        delete *it;
};

/**
 * add new edge to graph
 * add source and destination node if it's needed
 * @param source
 * @param destination
 */
void Graph::addEdge(int source, int destination){
    Node * s = getNode(source);
    Node * d = getNode(destination);
    if (s == 0) { // add new node
        s = addNode(source);
        this->nodes.push_back(s);
    }
    if (d == 0) { // add new node
        d = addNode(destination);
        this->nodes.push_back(d);
    }
    // set up the edge
    s->adj.push_back(d);
}

/**
 * get a node from current graph by its name
 * @param name
 * @return 0 if there isn't any node to be found
 */
Node * Graph::getNode(int name){
    Node * node = 0;
    // iterate over the vector
    for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
        if ((*it)->name == name) {
            node = (*it);
            break;
        }
    }
    return node;
}

/**
 * create and add new node to the graph
 * @param name
 * @return pointer to the new node
 */
Node * Graph::addNode(int name){
    Node * node;
    node = new Node;
    node->name = name;    
    return node;
}
    
/**
 * reset several data item of the node
 */
void Graph::reset(){
     for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
        (*it)->visisted = false;

        (*it)->distance = -1;
        (*it)->discovery = -1;
        (*it)->finish = -1;
    }
}

/**
 * start to process bfs algorithm from root node
 * @param root
 */
void Graph::bfs(Node * root){    
    // create a new queue
    vector<Node *> queue;
    int distance = 0;
    // push root to queue
    queue.push_back(root);
    // init the root's distance
    root->distance = 0;
    // dequeue the queue until it becomes empty 
    while (queue.empty() == false) {
        // get the head
        Node * current = queue.back();
        queue.pop_back();
        // this node has been visited 
        current->visisted = true;
        // set the distance from current node to next considered node
        distance = current->distance + 1;
        
        // iterate over adjacency node of current node
        for (vector<Node *>::iterator it = current->adj.begin() ; 
            it != current->adj.end(); ++it){
            if ((*it)->visisted == false) { // this node hasn't been visited
                (*it)->visisted = true;
                (*it)->distance = distance;
                (*it)->parent = current;
                
                // enqueue
                if (queue.empty()) { // this is for a simply usage  of queue
                    queue.push_back((*it));
                } else {
                    queue.insert(queue.begin(), (*it));
                }
            }
        }
    }
}

/**
 * find Node from the graph by its name
 * @param source
 */
void Graph::bfs(int source) {
  //  reset();
    //  reset related data item of all node 
    for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
        (*it)->visisted = false;
        (*it)->distance = -1;
        (*it)->parent = 0;
    }
    
    // find the right node then process it
    for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
        if ((*it)->name == source) {
            bfs((*it));
            break;
        }
    }
}

//start dshomoye//

/**
 * process dfs algorithm 
 * @param root current considered node
 * @param time current accessing time
 */
void Graph::dfs(Node* root, int & time) {
    // set discovery time 
    root->discovery = time;
    root->visisted = true;
    // increase time
    time++;
    // this node is not hung
    if (root->adj.empty() == false) {
        // iterate over its adjacency node
        for (vector<Node *>::iterator it = root->adj.begin() ; 
            it != root->adj.end(); ++it){
            if ((*it)->visisted == false) { // this node hasn't been visited
                (*it)->parent = root; //set its backlink
                dfs((*it), time); // recursive call
            }
        }
    }
    // set finishing time
    root->finish = time;
}

/**
 * process dfs algorithm
 * @param source name of the start node
 */
void Graph::dfs(int source) {
    //reset();
    
    // reset all related data of all nodes
    for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
        (*it)->visisted = false;

        (*it)->discovery = UNDEFINED;
        (*it)->finish = UNDEFINED;
    }
    // initialize  running time
    int time = 0;
    for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
        if ((*it)->name == source) { // find the right node to start
            dfs((*it), time);
            break;
        }
    }
}

/**
 * process Tarjan algorithm to find all strong connection components
 * @return numbere of strong connection components
 */
int Graph::tarjan() {
    // this for coloring the graph
    // and count the number of strong connection component
    connectionIndex = 0;
    // reset related data of all node
    for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
        (*it)->index = -1;
        (*it)->onStack = false;
    }
    
    // clear current vector of all strong connection component
    for (vector< vector<Node *>  >::iterator it = scc.begin() ; 
            it != scc.end(); ++it){
        (*it).clear();
    }
    scc.clear();
    
    // push all node to stack and connect them
    vector<Node *> stack;
    for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
        if ((*it)->index == UNDEFINED) {
            strongConnect((*it), stack);
        }
    }
    // setup color of each strong connection component 
    int color = 0;
    for (vector< vector<Node *>  >::iterator it = scc.begin() ; 
            it != scc.end(); ++it){
        if ((*it).empty() == false) {
            for (vector<Node *>::iterator it2 = (*it).begin() ; 
                it2 != (*it).end(); ++it2){
                (*it2)->color = color; 
            }
            color++;
        }
    }
    return color;
}

/**
 * find all related node that has strong connection to this node
 * @param v
 * @param stack
 */
void Graph::strongConnect(Node* v, vector<Node *> & stack ) {
    v->index = connectionIndex;
    v->lowlink = connectionIndex;
    connectionIndex += 1;
    stack.push_back(v);
    v->onStack = true;
    for (vector<Node *>::iterator it = v->adj.begin() ; 
            it != v->adj.end(); ++it){
        Node * w = (*it);
        if (w->index == UNDEFINED) {
            strongConnect(w, stack);
            v->lowlink = v->lowlink < w->lowlink ? v->lowlink : w->lowlink;
        } else if (w->onStack) {
            v->lowlink = v->lowlink < w->index ? v->lowlink : w->index;
        }
    }
    if (v->lowlink == v->index) {
        Node * w = 0;
        vector<Node *> sc; // strong connection
        do{
            w = stack.back();
            stack.pop_back();
            w->onStack = false;
            sc.push_back(w);
        } while (w != v) ;
        scc.push_back(sc);
    }

}

/**
 * find the shortest path in the graph
 * @param source
 * @param num
 * @return length of the path
 */
int Graph::dijkstra(int source, int num) {
    Node * s = getNode(source);
    Node * d = getNode(num);
    if (s == 0 
            || d == 0 
            || s == d) {
        return 0;
    } else {
        return dijkstra(s, d);       
    }
    
}

/**
 * find the shortest path in the graph
 * @param source
 * @param destination
 * @return length of the path
 */
int Graph::dijkstra(Node* source, Node* destination){
    vector<Node *> queue;
    // reset several data item of all node
    for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
        (*it)->dist = INT_MAX / 2;
        (*it)->prev = 0;
        (*it)->next = 0;
        queue.push_back((*it));
    }
    source->dist = 0;
    vector<Node *>::iterator mark;
    Node * u;
    
    while (queue.empty() == false) { // loop until all item has been considered
        // get the node that has minimum distance from source node
        u = 0;
        for (vector<Node *>::iterator it = queue.begin() ; 
            it != queue.end(); ++it){
            if (u == 0 
                    || u->dist > (*it)->dist ) {
                u = (*it);
                mark = it;
            }
        }
        // remove that node of queue
        queue.erase(mark);
        
        // update distance of all nodes that are in queue and in
        // adjacency list of u
        int alt = u->dist + 1;
        for (vector<Node *>::iterator it2 = queue.begin() ; 
            it2 != queue.end(); ++it2){
            for (vector<Node *>::iterator it = u->adj.begin() ; 
                it != u->adj.end(); ++it){                
                if ((*it2) == (*it) 
                        && alt < (*it)->dist) {
                    (*it)->dist = alt;
                    (*it)->prev = u;
                }                
            }
        }
           
    }

    
                ////start john ////
   

    // get the path length
    // set the pointer from source to destination
    int counter = 0;
    Node * cur = destination;
    Node * prev = 0;
    while (cur != 0) {
        cur->next = prev;
        prev = cur;
        cur = cur->prev;
        counter++;
    }
    return counter;
}

/**
 * set indirection casse
 * @param undir
 */
void Graph::setUndirected(bool undir) {
    if (undir) {
        isDirected = false;
        
    } else {
        isDirected = true;
    }
}

/**
 * read input from file 
 * @param inFile
 * @return 
 */
bool Graph::readInput(string inFile) {
    if (nodes.empty() == false) {
        return true;
    }

    if (inFile.length() == 0) {
        return false;
    }
    
    ifstream in;
    in.open(inFile.c_str(), ios::in);
    if (in.is_open() == false) {
        return false;
    }
    int source;
    int destination;
    if (isDirected) {
        while (in >> source >> destination) {
            addEdge(source, destination);
        }
    } else {
        while (in >> source >> destination) {
            addEdge(source, destination);
            if (source != destination) {
                addEdge(destination, source);
            }            
        }
    }

    
    in.close();
    return true;
}

/**
 * write bfs result to file 
 * @param outFile
 * @return 
 */
bool Graph::BFSToFile(string outFile) {
    ofstream out;
    out.open(outFile.c_str(), ios::out);
    if (out.is_open() == false) {
        return false;
    }
    
        out << "digraph G { \n";
    out << "node [shape=circle]; \n";
    
    // write all node
    for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
        
        out << (*it)->name << " [color=" << colors.at(2) 
                << ",label=\"" << (*it)->name << "\\n" ;
        
        if ((*it)->distance == UNDEFINED) {
            out << "inf";
        } else {
            out << (*it)->distance;
        }
        out << "\",style=filled];\n" ;
        
    }
    
    // write all edge
    if (isDirected) {
        for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
            for (vector<Node *>::iterator it2 = (*it)->adj.begin() ; 
                it2 != (*it)->adj.end(); ++it2){
                if ((*it2)->parent == (*it)) {
                    out << (*it)->name << " -> " << (*it2)->name << "[style=dashed];\n";
                } else {
                    out << (*it)->name << " -> " << (*it2)->name << ";\n";
                }                
            }
        }
    } else {
        bool ** matrix ;
        const unsigned int size = nodes.size();
        matrix = new bool * [size];
        adjMatrix(matrix, size);
        Node * source;
        Node * dest;
        for (unsigned int i = 0; i < nodes.size(); i++) {
            for (unsigned int j = i; j < nodes.size(); j++) {                
                if (matrix[i][j]) {
                   source = nodes[i];
                   dest = nodes[j];
                   if (source->parent == dest) {
                       out << dest->name << " -> " << source->name << "[style=dashed];\n"; 
                   } else if (source == dest->parent) {
                       out << source->name << " -> " << dest->name << "[style=dashed];\n";                    
                   } else {
                        out << source->name << " -> " << dest->name << "[dir=none];\n";  
                   }  
                }    
            }
            delete [] matrix[i];
        }
        delete [] matrix;
    }
    
    out << "\n}";
    out.close();
    
    
    return true;
}

/**
 * write dfs result to file 
 * @param outFile
 * @return 
 */
bool Graph::DFSToFile(string outFile) {
    ofstream out;
    out.open(outFile.c_str(), ios::out);
    if (out.is_open() == false) {
        return false;
    }
    out << "digraph G { \n";    
    out << "node [shape=circle]; \n";
    
    // write all node
    for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){

        out << (*it)->name << " [color=" << colors.at(14) 
                << ",label=\"" << (*it)->name << "\\n" ;
        if ((*it)->discovery == UNDEFINED) {
            out << "inf| inf";
        } else {
            out << (*it)->discovery << "| " << (*it)->finish ;
        }
                
        out  << "\",style=filled];\n" ;

    }
    
    // write all edge
    
    if (isDirected) {
       for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
            for (vector<Node *>::iterator it2 = (*it)->adj.begin() ; 
                it2 != (*it)->adj.end(); ++it2){
                if ((*it2)->parent == (*it)) {
                    out << (*it)->name << " -> " << (*it2)->name << "[style=dashed];\n";
                } else {
                    out << (*it)->name << " -> " << (*it2)->name << ";\n";
                }                
            }
        } 
    } else {
        bool ** matrix ;
        const unsigned int size = nodes.size();
        matrix = new bool * [size];
        adjMatrix(matrix, size);
        Node * source;
        Node * dest;
        for (unsigned int i = 0; i < nodes.size(); i++) {
            for (unsigned int j = i; j < nodes.size(); j++) {                
                if (matrix[i][j]) {
                   source = nodes[i];
                   dest = nodes[j];
                   if (source->parent == dest) {
                       out << dest->name << " -> " << source->name << "[style=dashed];\n"; 
                   } else if (source == dest->parent) {
                       out << source->name << " -> " << dest->name << "[style=dashed];\n";                    
                   } else {
                        out << source->name << " -> " << dest->name << "[dir=none];\n";  
                   }  
                }    
            }
            delete [] matrix[i];
        }
        delete [] matrix;
    }

    
    out << "\n}";
    out.close();
    return true;
}

/**
 * write scc result to file 
 * @param outFile
 * @return 
 */
bool Graph::SCCToFile(string outFile) {
    ofstream out;
    out.open(outFile.c_str(), ios::out);
    if (out.is_open() == false) {
        return false;
    }
    out << "digraph G { \n";    
    out << "node [shape=circle]; \n";
    
    // write all nodes
    for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){

        out << (*it)->name 
                << " [label=\"" << (*it)->name << "\"," 
                << "color=" << colors[(*it)->color % colors.size()]
                << ",style=filled];\n" ;
    }
    
    // write all edges
    if (isDirected) {
        for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
            for (vector<Node *>::iterator it2 = (*it)->adj.begin() ; 
                it2 != (*it)->adj.end(); ++it2){
                
                out << (*it)->name << " -> " << (*it2)->name << ";\n";
            }
        }
    } else {
        bool ** matrix ;
        const unsigned int size = nodes.size();
        matrix = new bool * [size];
        adjMatrix(matrix, size);
        for (unsigned int i = 0; i < nodes.size(); i++) {
            for (unsigned int j = i; j < nodes.size(); j++) {
                if (matrix[i][j]) {
                   out << nodes[i]->name << " -> " << nodes[j]->name << "[dir=none];\n";    
                }    
            }
            delete [] matrix[i];
        }
        delete [] matrix;
    }
    out << "\n}";
    out.close();
    return true;
}

/**
 * write shortest path result to file 
 * @param outFile
 * @return 
 */
bool Graph::SPToFile(string outFile) {
    ofstream out;
    out.open(outFile.c_str(), ios::out);
    if (out.is_open() == false) {
        return false;
    }
    out << "digraph G { \n";
    out << "node [shape=circle]; \n";
    
    // write node
    for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){

        out << (*it)->name << " [color=" << colors.at(6) 
                << ",label=\"" << (*it)->name 
                << "\",style=filled];\n" ;
    }
    
    // write edge
    if (isDirected) {
       for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
            for (vector<Node *>::iterator it2 = (*it)->adj.begin() ; 
                it2 != (*it)->adj.end(); ++it2){
                if ((*it)->next == (*it2)) {
                    out << (*it)->name << " -> " << (*it2)->name << "[style=dashed];\n";
                } else {
                    out << (*it)->name << " -> " << (*it2)->name << ";\n";
                }                
            }
        } 
    } else {
        bool ** matrix ;
        const unsigned int size = nodes.size();
        matrix = new bool * [size];
        adjMatrix(matrix, size);
        Node * source;
        Node * dest;
        for (unsigned int i = 0; i < nodes.size(); i++) {
            for (unsigned int j = i; j < nodes.size(); j++) {                
                if (matrix[i][j]) {
                   source = nodes[i];
                   dest = nodes[j];
                   if (source == dest->next) {
                       out << dest->name << " -> " << source->name << "[style=dashed];\n"; 
                   } else if (source->next == dest) {
                       out << source->name << " -> " << dest->name << "[style=dashed];\n";                    
                   } else {
                        out << source->name << " -> " << dest->name << "[dir=none];\n";  
                   }  
                }    
            }
            delete [] matrix[i];
        }
        delete [] matrix;
    }
    
    out << "\n}";
    out.close();
    return true;
}

/**
 * write graph to file 
 * @param outFile
 * @return 
 */
bool Graph::ToFile(string outFile) {
    ofstream out;
    out.open(outFile.c_str(), ios::out);
    if (out.is_open() == false) {
        return false;
    }
    
    // write nodes
    if (isDirected) {
        out << "digraph G { \n";
    } else {
        out << "graph G { \n";
    }
    out << "node [shape=circle]; \n";

    
    for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
        out << (*it)->name << " [color=" << colors.at(7) 
                << ",label=\"" << (*it)->name 
                << "\",style=filled];\n" ;
    }
    
    // write all edge
    if (isDirected) {
        for (vector<Node *>::iterator it = nodes.begin() ; 
            it != nodes.end(); ++it){
            for (vector<Node *>::iterator it2 = (*it)->adj.begin() ; 
                it2 != (*it)->adj.end(); ++it2){
                
                out << (*it)->name << " -> " << (*it2)->name << ";\n";
            }
        }
    } else {
        bool ** matrix ;
        const unsigned int size = nodes.size();
        matrix = new bool * [size];
        adjMatrix(matrix, size);
        for (unsigned int i = 0; i < nodes.size(); i++) {
            for (unsigned int j = i; j < nodes.size(); j++) {
                if (matrix[i][j]) {
                   out << nodes[i]->name << " -- " << nodes[j]->name << ";\n";    
                }    
            }
            delete [] matrix[i];
        }
        delete [] matrix;
    }
    
    out << "\n}";
    out.close();
   // str += "\n}";
    return true;
}

/**
 * generate adjacency matrix from current vector of nodes
 * @param matrix
 * @param size
 */
void Graph::adjMatrix(bool ** matrix, int size) {
    
    for (unsigned int i = 0; i < nodes.size(); i++) {
        matrix[i] = new bool[size];
        for (unsigned int j = 0; j < nodes.size(); j++) {
            matrix[i][j] = false;
        }
    }
    int nameDest;
    Node * source;
    for (unsigned int i = 0; i < nodes.size(); i++) {

        source = nodes.at(i);
        for (vector<Node *>::iterator it2 = source->adj.begin() ; 
            it2 != source->adj.end(); ++it2){
            nameDest = (*it2)->name;
            for (unsigned int j = 0; j < nodes.size(); j++) {
                if (nameDest == nodes[j]->name) {
                    matrix[i][j] = true;
                    break;
                } // end if name matching
            }// end for node 
        } // end for adj vector            
    } // end for node
}

