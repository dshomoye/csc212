/* interface for graph class. */

/* TODO: write this.  How to represent the graph is up to you.
 * You might consider using STL vectors or lists.
 * */

#include <vector>
#include <cstdlib>
#include <cmath>
#include <climits>
#include <string>
#include <sstream>
using namespace std;

const int UNDEFINED  = -1;


struct Node{
    int name;
    vector<Node *> adj;
    
    // all  case uses these data
    bool visisted;
    Node * parent;
    
    // dfs only
    int discovery;
    int finish;
    
    // bfs only
    int distance;
    
    // scc only
    int index;
    int lowlink;
    bool onStack;
    int color;
    
    // shortest path only
    int dist;
    Node * prev;
    Node * next;
    
    
};



class Graph{
public:
    Graph();
    ~Graph();
    bool readInput(string inFile );
    void bfs(int source);
    void dfs(int source);
    int tarjan();
    int dijkstra(int source , int num);
    void setUndirected(bool undir);
    bool BFSToFile(string outFile);
    bool DFSToFile(string outFile);
    bool SCCToFile(string outFile); // strong connection component
    bool SPToFile(string outFile); // shortest path
    bool ToFile(string outFile); // graph to file
private:
    void addEdge(int source, int destination);
    Node * getNode(int name);
    Node * addNode(int name);
    void bfs(Node * root);
    void dfs(Node * root, int & time);
    void reset();
    void strongConnect(Node * v,  vector<Node *> & stack );
    int  dijkstra(Node * source , Node * destination );
    void adjMatrix(bool ** matrix, int size);
private:
    
    int connectionIndex; // for strong connection component
    vector< vector<Node *>  > scc; // strong connection component
    vector<Node *> nodes;
    bool isDirected; /// is directed graph
    vector<string> colors; // for strong connection component
};
