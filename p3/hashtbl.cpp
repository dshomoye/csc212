/****************************************************
implementation file for the hash table class.
*****************************************************/

#include "hashtbl.h"
#include <iostream>
using std::endl;
using std::ostream;
#include <string.h> /* for strcpy */
#include <algorithm> /* for find */
using std::find;
#include <iomanip>
using std::setfill;
using std::setw;
#include <cassert>

#define R32 (rand()*100003 + rand())
#define R64 (((uint64_t)R32 << 32) + R32)
#define TLEN ((size_t)(1 << this->nBits))

namespace csc212
{
	/* First we implement our hash family */
	hashTbl::hash::hash(unsigned rangebits, const uint32_t* a,
					const uint64_t* alpha, const uint64_t* beta) {
		this->rangebits = rangebits;
		if (a) {
			for (size_t i = 0; i < aLen; i++) {
				this->a[i] = a[i] | 1; /* make sure it is odd. */
			}
		} else {
			for (size_t i = 0; i < aLen; i++) {
				this->a[i] = R32 | 1;
			}
		}
		this->alpha = ((alpha) ? *alpha : R64) | 1;
		this->beta = ((beta) ? *beta : R64);
		/* only keep the low order bits of beta: */
		this->beta &= ((uint64_t)-1)>>(64 - rangebits);
	}
	uint64_t hashTbl::hash::operator()(const string& x) const {
		assert(x.length() <= 4*aLen);
		/* TODO: write the hash function. */
		int hash = 0;
		int index;
		for(unsigned int i = 0; i < x.length(); i++) {
			hash = hash + (int)x[i];
		}
		index = hash % aLen; //TLEN Doesn't work!??! 
		return index;
	}

	//constructors:
	hashTbl::hashTbl(unsigned nBits, const uint32_t* ha,
					const uint64_t* halpha, const uint64_t* hbeta) :
		nBits(nBits),h(nBits,ha,halpha,hbeta)
	{
		this->table = new list<val_type>[TLEN];
	}
	hashTbl::hashTbl(const hashTbl& H)
	{
		/* TODO: write this */
		/* NOTE: the underlying linked list class has a working
		 * assignment operator! */
            nBits = H.nBits;
            h = H.h;
            /* new array*/
            this->table = new list<val_type>[TLEN];
            /* copy each item */
            for (int i = 0; i < TLEN; i++) {
                for (std::list<val_type>::iterator it= H.table[i].begin(); it != H.table[i].end(); ++it)
                    this->table[i].push_back(*it);
            }
	}

	//destructor:
	hashTbl::~hashTbl()
	{
		delete[] this->table;
		//NOTE: this will call the destructor of each of the linked lists,
		//so there isn't anything else that we need to worry about.
	}

	//operators:
	hashTbl& hashTbl::operator=(hashTbl H)
	{
		/* TODO: write this */
		for (size_t i=0; i<TLEN; i++){
            this-> table[i]= H.table[i];
         }
		return *this;
	}

	ostream& operator<<(ostream& o, const hashTbl& H)
	{
		for (size_t i = 0; i < H.tableLength(); i++) {
			o << "[" << setfill('0') << setw(2) << i << "] |";
			for (list<val_type>::iterator j = H.table[i].begin();
					j != H.table[i].end(); j++) {
				o << *j << "|";
			}
			o << endl;
		}
		return o;
	}

	void hashTbl::insert(val_type x)
	{
		/* TODO: write this */
		hashTbl::hash k;
      uint64_t has= k(x);
      
      for (size_t i=0; i< TLEN; i++)
      {		
		if ( find (this-> table[i].begin(), this-> table[i].end(), x ) != this-> table[i].end() )
		{ 
            return;
        }
      }
     
    this->table[has].push_back(x);
		//Remember to check for uniqueness before inserting.
	}

	void hashTbl::remove(val_type x)
	{
		/* TODO: write this */
            size_t i;
            for(i=0; i<TLEN; i++)
            	/* iterate over the array*/
                for (list<val_type>::iterator j = this->table[i].begin();
                            j != this->table[i].end(); j++)
                            	/* find in the list */
                    if (x == *j) {
                        this->table[i].remove(x);
                        break;
                    }

	}

	void hashTbl::clear()
	{
		for(size_t i=0; i<TLEN; i++)
			this->table[i].clear();
	}

	bool hashTbl::isEmpty() const
	{
		/* TODO: write this */
		/* the table is empty when all list are empty */
            for(size_t i=0; i<TLEN; i++)
                if (this->table[i].empty() == false) {
                    return false;
                }

		return true; // just so it compiles...
	}

	bool hashTbl::search(val_type x) const
	{
		/* TODO: write this */
           /* hash h;
            uint64_t t = h(x);
            size_t index = t % tableLength();
            for (list<val_type>::iterator j = this->table[index].begin();
                            j != this->table[index].end(); j++) {
                if (x == *j) {
                    return true;
                }

            }*/
            size_t i;
            for(i=0; i<TLEN; i++)
            	/* llok for each list in the table*/
                for (list<val_type>::iterator j = this->table[i].begin();
                            j != this->table[i].end(); j++)
                            	/* look inside each list*/
                    if (x == *j)
                        return true;


		return false; // just so it compiles...
	}

	size_t hashTbl::countElements() const
	{
		/* TODO: write this */
		size_t count = 0;
		int * ptr;
		for(size_t i = 0; i<TLEN; i++) {
			if(table[i].size() == 1) {
				count++;
			}
			else if(table[i].size() > 1) {
				count++;
				ptr = table[i]; //Pretty sure this is wrong but
				while(this->ptr != NULL) {//Linked list, while not at end
					count++; //Add to count
					this->ptr = ptr->next;//Move to the next element
				}
			}
			else; //If its empty, do nothing and continue looping
		}
		return count;
	}

	size_t hashTbl::tableLength() const
	{
		return TLEN;
	}

	size_t hashTbl::countCollisions() const
	{
		//just count the number of lists of length > 1
		size_t i,nCollisions=0;
		for(i=0; i<TLEN; i++)
			if(table[i].size() > 1)
				++nCollisions;
		return nCollisions;
	}

	size_t hashTbl::longestListLength() const
	{
		/* TODO: write this */
		size_t l = 0;
		for (size_t i = 0; i<TLEN; i++){
			if(table[i].size()>l){
				l=table[i].size();
			}
		}
		return l; // just so it compiles...
	}
}
